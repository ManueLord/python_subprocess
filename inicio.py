import subprocess
import os 
  
def excuteC(): 
    s = subprocess.check_call("gcc c_he.c -o out1;./out1", shell = True) 
    print(", return code", s) 
  
def executeCpp(): 
    data, temp = os.pipe() 
    os.write(temp, bytes("5 10\n", "utf-8")); 
    os.close(temp) 
    s = subprocess.check_output("g++ cpp_he.cpp -o out2;./out2", stdin = data, shell = True) 
    print(s.decode("utf-8")) 
  
def executeJava(): 
    data, temp = os.pipe() 
    os.write(temp, bytes("5 10\n", "utf-8")); 
    os.close(temp) 
    s = subprocess.check_output("javac java_he.java;java HelloWorld", stdin = data, shell = True) 
    print(s.decode("utf-8")) 
  
  
if __name__=="__main__": 
    excuteC()     
    executeCpp() 
    executeJava()